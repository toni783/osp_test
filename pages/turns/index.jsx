import React, { memo, useState, useEffect } from 'react';
import {
  Col,
  Row,
  Card,
  Button,
  CardBody,
  CardText,
  FormGroup,
  Label,
  Input,
  FormFeedback,
} from 'reactstrap';
import CustomTable from '../../components/table/CustomTable';
import CustomModal from '../../components/modal/CustomModal';

import MainLayout from '../../layout/MainLayout';
import HeadDefault from '../../layout/head/HeadDefault';
import { Field, Form, Formik } from 'formik';
import * as Yup from 'yup';
import TableActions from '../../components/table/tableActions/TableActions';
const TurnsPage = memo(props => {
  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);
  const [data, setData] = useState([]);
  const [editMode, setEditMode] = useState(false);
  const [currentSelected, setCurrentSelected] = useState(null);
  const [initialValues, setInitialValues] = useState({
    turn: new Date().toISOString().slice(0, 10),
    status: false,
  });

  useEffect(() => {
    if (localStorage.getItem('turns')) {
      setData(JSON.parse(localStorage.getItem('turns')));
    }
  }, []);

  const FormSchema = Yup.object().shape({
    turn: Yup.date().required(),
    status: Yup.boolean(),
  });

  const saveToLocalStorage = data => {
    localStorage.setItem('turns', JSON.stringify(data));
  };

  const onSaveMovies = ({ turn, status }, resetForm) => {
    const payload = { id: Math.random(), turn, status };
    const dataCp = [...data];
    if (editMode) {
      dataCp.push(payload);
    } else {
      const currentIndex = dataCp.findIndex(val => {
        return val.id === currentSelected.id;
      });
      dataCp[currentIndex] = payload;
    }

    saveToLocalStorage(dataCp);
    setData(dataCp);
    toggle();
    resetForm();
  };

  const onEdit = id => {
    toggle();
    const [result] = data.filter(val => val.id === id);
    const { turn, status } = result;
    setInitialValues({ turn, status });
    setEditMode(false);
    setCurrentSelected(result);
  };

  const onLock = id => {
    // TODO: Lock item (not part of the requirement)
    alert('Lock item!');
  };
  const onDelete = id => {
    const dataCp = data.filter(val => val.id !== id);
    setData(dataCp);
    saveToLocalStorage(dataCp);
  };

  const onOpenModal = () => {
    setEditMode(true);
    toggle();
    setInitialValues({
      turn: new Date().toISOString().slice(0, 10),
      status: false,
    });
  };

  const buttonFormatter = (cell, row) => {
    return (
      <TableActions
        edit
        lock
        delete
        onEdit={() => {
          onEdit(row.id);
        }}
        onLink={() => {
          onLink(row.id);
        }}
        onLock={() => {
          onLock(row.id);
        }}
        onDelete={() => {
          onDelete(row.id);
        }}
      />
    );
  };

  const columns = [
    {
      dataField: 'id',
      text: 'ID ',
    },
    {
      dataField: 'turn',
      text: 'Turno',
    },
    {
      dataField: 'status',
      text: 'Estado',
    },

    {
      dataField: 'actions',
      text: 'Acciones',
      isDummyField: true,
      formatter: buttonFormatter,
    },
  ];

  return (
    <>
      <HeadDefault
        title="Profile | Next.JS with Reactstrap (React dashboard web application)"
        description="NextJS with Reactstrap components with SCSS library, a NextJS dashboard template."
      />
      <MainLayout activeLink="turns">
        <Row>
          <Col xs="9">
            <h1>Turnos</h1>
          </Col>
          <Col xs="3">
            <Button
              onClick={() => {
                onOpenModal();
              }}
              outline
              color="primary"
            >
              Nuevo Turno &nbsp;<i className="fas fa-plus"></i>
            </Button>
          </Col>
        </Row>
        <Row>
          <Col xs="12">
            <Card>
              <CardBody>
                <CardText>
                  <CustomTable data={data} columns={columns} />
                </CardText>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <CustomModal
          isOpen={modal}
          toggle={toggle}
          title={editMode ? 'Nueva Turno' : 'Editar Turno'}
        >
          <Formik
            enableReinitialize={true}
            initialValues={initialValues}
            onSubmit={(values, { resetForm }) => {
              onSaveMovies(values, resetForm);
            }}
            validationSchema={FormSchema}
          >
            {({ dirty, isValid, errors, touched }) => {
              return (
                <Form>
                  <FormGroup>
                    <Label for="turn">Turno</Label>

                    <Field
                      invalid={errors.turn && touched.turn}
                      type="date"
                      name="turn"
                      id="turn"
                      placeholder="Ingrese Turno"
                      required
                      as={Input}
                    />
                    <FormFeedback>Campo Requerido!</FormFeedback>
                  </FormGroup>

                  <FormGroup check>
                    <Label check>
                      <Field
                        invalid={errors.status && touched.status}
                        type="checkbox"
                        name="status"
                        id="status"
                        as={Input}
                      />
                      Activo
                    </Label>
                  </FormGroup>

                  <FormGroup className="d-flex justify-content-center ">
                    <Button color="primary" type="submit">
                      {editMode ? 'Guardar' : 'Editar'}
                    </Button>
                  </FormGroup>
                </Form>
              );
            }}
          </Formik>
        </CustomModal>
      </MainLayout>
    </>
  );
});

export default TurnsPage;
