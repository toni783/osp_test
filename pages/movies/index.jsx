import React, { memo, useState, useEffect } from 'react';
import {
  Col,
  Row,
  Card,
  Button,
  CardBody,
  CardText,
  FormGroup,
  Label,
  Input,
  FormFeedback,
} from 'reactstrap';
import CustomTable from '../../components/table/CustomTable';
import CustomModal from '../../components/modal/CustomModal';

import MainLayout from '../../layout/MainLayout';
import HeadDefault from '../../layout/head/HeadDefault';
import { Field, Form, Formik } from 'formik';
import * as Yup from 'yup';
import TableActions from '../../components/table/tableActions/TableActions';

const MoviesPage = memo(props => {
  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);
  const [linkModal, setLinkModal] = useState(false);
  const toggleLinkModal = () => setLinkModal(!linkModal);
  const [data, setData] = useState([]);
  const [turnsData, setTurnsData] = useState([]);
  const [editMode, setEditMode] = useState(false);
  const [currentSelected, setCurrentSelected] = useState(null);
  const [initialValues, setInitialValues] = useState({
    name: '',
    createdAt: new Date().toISOString().slice(0, 10),
    status: 'active',
  });

  useEffect(() => {
    if (localStorage.getItem('movies')) {
      setData(JSON.parse(localStorage.getItem('movies')));
    }

    if (localStorage.getItem('turns')) {
      setTurnsData(JSON.parse(localStorage.getItem('turns')));
    }
  }, []);

  const FormSchema = Yup.object().shape({
    name: Yup.string().required(),
    createdAt: Yup.date().required(),
    status: Yup.string().required(),
  });

  const saveToLocalStorage = data => {
    localStorage.setItem('movies', JSON.stringify(data));
  };

  const onSaveMovies = ({ createdAt, name, status }, resetForm) => {
    const payload = { id: Math.random(), createdAt, name, status };
    const dataCp = [...data];
    if (editMode) {
      dataCp.push(payload);
    } else {
      console.log(payload);
      const currentIndex = dataCp.findIndex(val => {
        return val.id === currentSelected.id;
      });
      dataCp[currentIndex] = payload;
      console.log(dataCp[currentIndex]);
    }

    saveToLocalStorage(dataCp);
    setData(dataCp);
    toggle();
    resetForm();
  };

  const onEdit = id => {
    console.log('edit');
    setModal(true);
    const [result] = data.filter(val => val.id === id);
    const { createdAt, name, status } = result;
    setInitialValues({ createdAt, name, status });
    setEditMode(false);
    setCurrentSelected(result);
  };

  const onLink = id => {
    const [result] = data.filter(val => val.id === id);
    if (result.turn) {
      alert('Error! Esta pèlicula ya tiene un turno asignado!');
      return;
    } else {
      toggleLinkModal();
      setCurrentSelected(result);
    }
  };

  const onLockLink = id => {
    const dataCp = [...data];
    const currentIndex = dataCp.findIndex(val => {
      return val.id === currentSelected.id;
    });
    const [resultTurn] = turnsData.filter(val => val.id === id);
    dataCp[currentIndex].turn = resultTurn;
    saveToLocalStorage(dataCp);
    setData(dataCp);
    toggleLinkModal();
    alert('Turno asignado exitosamente!');
  };
  const onLock = id => {
    // TODO: Lock item (not part of the requirement)
    alert('Lock item!');
  };
  const onDelete = id => {
    const dataCp = data.filter(val => val.id !== id);
    setData(dataCp);
    saveToLocalStorage(dataCp);
  };

  const onOpenModal = () => {
    setEditMode(true);
    toggle();
    setInitialValues({
      name: '',
      createdAt: new Date().toISOString().slice(0, 10),
      status: 'active',
    });
  };

  const buttonFormatter = (cell, row) => {
    return (
      <TableActions
        edit
        link
        lock
        delete
        onEdit={() => {
          onEdit(row.id);
        }}
        onLink={() => {
          onLink(row.id);
        }}
        onLock={() => {
          onLock(row.id);
        }}
        onDelete={() => {
          onDelete(row.id);
        }}
      />
    );
  };

  const buttonFormatterLink = (cell, row) => {
    return (
      <TableActions
        lock
        onLock={() => {
          onLockLink(row.id);
        }}
      />
    );
  };
  const columns = [
    {
      dataField: 'id',
      text: 'ID ',
    },
    {
      dataField: 'name',
      text: 'Nombre',
    },

    {
      dataField: 'createdAt',
      text: 'F.Publicación',
    },

    {
      dataField: 'status',
      text: 'Estado',
      formatter(cell, row) {
        return row.status === 'active' ? (
          <span>Activo</span>
        ) : (
          <span>Inactivo</span>
        );
      },
    },
    {
      dataField: 'actions',
      text: 'Acciones',
      isDummyField: true,
      formatter: buttonFormatter,
    },
  ];

  const turnsColumns = [
    {
      dataField: 'id',
      text: 'ID ',
    },
    {
      dataField: 'turn',
      text: 'Turno',
    },
    {
      dataField: 'status',
      text: 'Estado',
      formatter(cell, row) {
        return row.status === true ? (
          <span>Activo</span>
        ) : (
          <span>Inactivo</span>
        );
      },
    },

    {
      dataField: 'actions',
      text: 'Acciones',
      isDummyField: true,
      formatter: buttonFormatterLink,
    },
  ];

  return (
    <>
      <HeadDefault title="Peliculas" description="Pagina de Peliculas." />
      <MainLayout activeLink="movies">
        <Row>
          <Col xs="9">
            <h1>Peliculas</h1>
          </Col>
          <Col xs="3">
            <Button
              onClick={() => {
                onOpenModal();
              }}
              outline
              color="primary"
            >
              Nueva Pelicula &nbsp;
              <i className="fas fa-plus"></i>
            </Button>
          </Col>
        </Row>
        <Row>
          <Col xs="12">
            <Card>
              <CardBody>
                <CardText>
                  <CustomTable data={data} columns={columns} />
                </CardText>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <CustomModal
          isOpen={modal}
          toggle={toggle}
          title={editMode ? 'Nueva Pelicula' : 'Editar Pelicula'}
        >
          <Formik
            enableReinitialize={true}
            initialValues={initialValues}
            onSubmit={(values, { resetForm }) => {
              onSaveMovies(values, resetForm);
            }}
            validationSchema={FormSchema}
          >
            {({ dirty, isValid, errors, touched }) => {
              return (
                <Form>
                  <FormGroup>
                    <Label for="name">Nombre de Pelicula</Label>

                    <Field
                      invalid={errors.name && touched.name}
                      type="text"
                      name="name"
                      id="name"
                      placeholder="Ingrese Pelicula"
                      required
                      as={Input}
                    />
                    <FormFeedback>Campo Requerido!</FormFeedback>
                  </FormGroup>

                  <FormGroup>
                    <Label for="name">Fecha de Publicaci&oacute;n</Label>

                    <Field
                      invalid={errors.createdAt && touched.createdAt}
                      type="date"
                      name="createdAt"
                      id="createdAt"
                      placeholder="Ingrese Pelicula"
                      required
                      as={Input}
                    />
                    <FormFeedback>Campo Requerido!</FormFeedback>
                  </FormGroup>

                  <FormGroup>
                    <Label for="name">Estado</Label>

                    <Field
                      invalid={errors.status && touched.status}
                      type="select"
                      name="status"
                      id="status"
                      placeholder="Seleccione Estado"
                      required
                      as={Input}
                    >
                      <option value="active">Activo</option>
                      <option value="inactive">Inactivo</option>
                    </Field>

                    <FormFeedback>Campo Requerido!</FormFeedback>
                  </FormGroup>

                  <FormGroup className="d-flex justify-content-center ">
                    <Button
                      color="primary"
                      disabled={(!dirty && editMode) || !isValid}
                      type="submit"
                    >
                      {editMode ? 'Guardar' : 'Editar'}
                    </Button>
                  </FormGroup>
                </Form>
              );
            }}
          </Formik>
        </CustomModal>

        <CustomModal
          isOpen={linkModal}
          toggle={() => {
            toggleLinkModal();
          }}
          title="Nueva Pelicula"
        >
          <CustomTable data={turnsData} columns={turnsColumns} />
        </CustomModal>
      </MainLayout>
    </>
  );
});

export default MoviesPage;
