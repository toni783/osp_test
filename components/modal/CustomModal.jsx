import React from 'react';
import {
  Col,
  Row,
  Card,
  Button,
  CardBody,
  CardText,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
} from 'reactstrap';

const CustomModal = props => {
  return (
    <Modal isOpen={props.isOpen} toggle={props.toggle}>
      <ModalHeader toggle={props.toggle}>{props.title}</ModalHeader>
      <ModalBody>{props.children}</ModalBody>
    </Modal>
  );
};

export default CustomModal;
