import React, { memo } from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';

const CustomTable = memo(props => {
  const customCaret = (order, column) => {
    if (!order)
      return (
        <span>
          &nbsp;&nbsp;
          <i className="fas fa-arrow-up"></i> /{' '}
          <i className="fas fa-arrow-down"></i>
        </span>
      );
    else if (order === 'asc')
      return (
        <span>
          &nbsp;&nbsp;
          <i className="fas fa-arrow-up fa-active"></i> /{' '}
          <i className="fas fa-arrow-down"></i>
        </span>
      );
    else if (order === 'desc')
      return (
        <span>
          &nbsp;&nbsp;
          <i className="fas fa-arrow-up"></i> /{' '}
          <i className="fas fa-arrow-down fa-active"></i>
        </span>
      );
    return null;
  };

  return (
    <>
      <BootstrapTable
        keyField="id"
        data={props.data}
        columns={props.columns.map(val => {
          val.sort = true;
          val.sortCaret = customCaret;
          return val;
        })}
        pagination={paginationFactory()}
      />
    </>
  );
});

export default CustomTable;
