import React from 'react';
import { Button } from 'reactstrap';

const TableActions = props => {
  return (
    <div>
      {props.edit ? (
        <Button onClick={props.onEdit} outline color="primary">
          <i className="fas fa-edit"></i>
        </Button>
      ) : null}

      {props.link ? (
        <Button onClick={props.onLink} outline color="secondary">
          <i className="fas fa-bars"></i>
        </Button>
      ) : null}

      {props.lock ? (
        <Button onClick={props.onLock} outline color="warning">
          <i className="fas fa-lock"></i>
        </Button>
      ) : null}

      {props.delete ? (
        <Button onClick={props.onDelete} outline color="danger">
          <i className="fas fa-trash"></i>
        </Button>
      ) : null}
    </div>
  );
};

export default TableActions;
