import React, { memo } from 'react';

const HomePage = memo(props => {
  return (
    <>
      <div className="hero-start">
        <h1 className="title">Welcome to Gilbert Morett Test</h1>
      </div>
    </>
  );
});

export default HomePage;
