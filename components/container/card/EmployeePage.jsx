import React, { memo } from 'react';

import { EmployeeCard, EmployeeVCard } from '../../card';
import { CARDS } from '../../../constants/cards';

const EmployeePage = memo(props => {
  const { dispatch, storeLayout, id } = props;

  return (
    <>
      <h1>Admins</h1>
      <h4>Mock Data</h4>
      <EmployeeCard items={CARDS.employees} />
      <div className="d-block clearfix"></div>
    </>
  );
});

export default EmployeePage;
