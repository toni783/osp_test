# Test OSP

This project was generated from the seed [nextjs-reactstrap](https://github.com/dyarfi/nextjs-reactstrap)

### Prerequisites

```
 Node.js version 10.x or greater

To check your version, run node -v in a terminal/console window.

To get Node.js, go to nodejs.org.
```

### Features

_`Fully responsive with custom mobile design`
_`Added custom tables with functionalities`
_`Custom Sort on tables for fields`
_`Using functional components`
\_`Using LocalStorage for storing data`
\_`Live preview on Vercel`
\_`Using NextJS`

### Online demo

[Demo](https://osp-test.vercel.app/)

## Development server

Run `npm run dev` for a dev server. Navigate to `http://localhost:3001/`. The app will automatically reload if you change any of the source files.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `.next` directory.

## Authors

- **Gilbert Morett** - _Repo_ - [Bitbucket](https://bitbucket.org/toni783/)
