// Menus
const MENUS = [
  {
    name: 'dashboard',
    as: 'a',
    href: '/dashboard',
    label: 'Dashboard',
    icon: 'fas fa-home',
  },
  {
    name: 'movies',
    as: 'a',
    href: '/movies',
    label: 'Peliculas',
    icon: 'fas fa-film',
  },
  {
    name: 'turns',
    as: 'a',
    href: '/turns',
    label: 'Turnos',
    icon: 'fas fa-check-square',
  },
  {
    name: 'admins',
    as: 'a',
    href: '/admins',
    label: 'Administradores',
    icon: 'fas fa-tools',
  },
  {
    name: 'profile',
    as: 'a',
    href: '/profile',
    label: 'Perfil',
    icon: 'fas fa-user-circle',
  },
  {
    name: 'close',
    as: 'a',
    href: '',
    label: 'Cerrar Sesión',
    icon: 'fas fa-times-circle',
  },
];

export { MENUS };
