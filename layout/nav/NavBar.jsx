import React from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText,
} from 'reactstrap';
import Link from 'next/link';

function NavBar({
  /* state vars */
  isOpen,
  /* toggles */
  toggle,
}) {
  return (
    <div>
      <Navbar color="dark" dark expand="sm" fixed="top">
        <Link href="/">
          <NavbarBrand href="/">
            <img src="/logo.svg" alt="Logo" className="logo" />
            Evaluaci&oacute;n OSP
          </NavbarBrand>
        </Link>

        <NavbarToggler onClick={toggle} color="dark" />
        <Collapse isOpen={isOpen} navbar>
          <UncontrolledDropdown inNavbar>
            <DropdownToggle caret nav className="text-secondary">
              <NavbarText className="align-self-center text-left font-weight-bold">
                <img
                  src="/images/profile2.jpg"
                  className="border rounded-circle img-42 img-fluid mr-1"
                />
                John Doe
              </NavbarText>
            </DropdownToggle>
            <DropdownMenu>
              <DropdownItem tag="div">
                <Link href="/profile" className="text-dark">
                  <span>
                    <i className="fas fa-user-circle"></i> Perfil
                  </span>
                </Link>
              </DropdownItem>
              <DropdownItem tag="div">
                <Link href="/" className="text-dark">
                  <span>
                    <i className="fas fa-times-circle"></i> Cerrar Sesi&oacute;n
                  </span>
                </Link>
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </Collapse>
      </Navbar>
    </div>
  );
}

export default NavBar;
