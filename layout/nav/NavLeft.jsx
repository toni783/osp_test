import React from 'react';
import { ListGroup, ListGroupItem } from 'reactstrap';
import Link from 'next/link';

import { MENUS } from '../../constants/menus';

function NavLeft(props) {
  const { activeLink } = props;
  return (
    <>
      <h4 className="headline">Components</h4>
      <div>
        <ListGroup flush className="list-group-nav-left" tag="div">
          {MENUS.map((item, k) => {
            const isActive = activeLink === item.name ? true : false;
            return (
              <Link key={`l${k}`} href={item.href}>
                <ListGroupItem active={isActive} tag={item.as}>
                  {item.icon && <i className={item.icon}></i>} {item.label}
                </ListGroupItem>
              </Link>
            );
          })}
        </ListGroup>
      </div>
    </>
  );
}

export default NavLeft;
